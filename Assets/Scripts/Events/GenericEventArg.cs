
public class GenericEventArg<T> :System.EventArgs
{
    public T Data;
    public GenericEventType EventType;
    public GenericEventArg(T data)
        {
        Data = data;
        }
    public GenericEventArg(T data, GenericEventType eventType)
    {
        Data = data;
        EventType = eventType;
    }
    static GenericEventArg<T> ConvertToArgs(T Data)
    { 
        return new GenericEventArg<T>(Data);
    }
}
public enum GenericEventType
{ 
OnGameStateChanged,
OnContainerAdd,
OnContainerRemove,
OnContainerFillStateChanged,

}

