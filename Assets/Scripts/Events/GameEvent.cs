using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameEvent", menuName = "ScriptableObject/Event/GameEvent")]
public class GameEvent : ScriptableObject
{
    HashSet<ActionFunction> _Actions = new HashSet<ActionFunction>();
    HashSet<IGameEventListener> _Listeners = new HashSet<IGameEventListener>();
    public delegate void ActionFunction(EventArgs eventArgs);

    public void Invoke(EventArgs args = null)
    {
        foreach (IGameEventListener listener in _Listeners)
            listener.RaiseEvent(args);

        foreach (ActionFunction action in _Actions)
            action.Invoke(args);

    }
    public void Register(IGameEventListener listener) => _Listeners.Add(listener);

    public void RegisterAction(ActionFunction action) => _Actions.Add(action);

    public void Unregister(IGameEventListener listener) => _Listeners.Remove(listener);

    public void UnregisterAction(ActionFunction action) => _Actions.Remove(action);
}

