using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameManager : MonoBehaviour
{
    int PlayerLevel => PlayerPrefs.GetInt("PlayerLevel", 0);
    [SerializeField] GameEvent StartGameEvent;
    [SerializeField] GameEvent LevelLoadedEvent;
    [SerializeField] LevelList _LevelList;
     GameObject LoadedLevel;
    public int LevelToLoad
    {
        get {

            if (_LevelList != null && _LevelList.Count > 0)
                return Mathf.Clamp(PlayerLevel ,0, _LevelList.Count-1);
            else 
                return -1;
        }
    }
    public void NextLevel()
    {
        PlayerPrefs.SetInt("PlayerLevel", PlayerLevel + 1);


        RestartLevel();
    }
    public void RestartLevel()
    {
        StartCoroutine(Restart());
    }
    public void StartLevel()
    {
        StartGameEvent.Invoke();
    }

    private void Start()
    {
        LoadLevel();
    }

    public void UnloadLevel()
    {
        LoadedLevel.SetActive(false);
        Destroy(LoadedLevel.gameObject);
 
    }


    public void LoadLevel()
    {
        if (LoadedLevel != null)
        {
            RestartLevel();
            return;
        }

        if (LevelToLoad >= 0)
            LoadedLevel = Instantiate(_LevelList.TheList[LevelToLoad]);
        else
            Debug.LogError("LevelList is null or empty");

        LevelLoadedEvent.Invoke();
    }

    IEnumerator Restart()
    {
        UnloadLevel();
        while(LoadedLevel!=null) 
            yield return new WaitForEndOfFrame();
       
        LoadLevel();
    }

}
