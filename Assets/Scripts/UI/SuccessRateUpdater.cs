using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuccessRateUpdater : MonoBehaviour
{
    [SerializeField] GameEvent OnSuccessRateChange;
    [SerializeField] GameEvent OnSceneLoadComplete;
    [SerializeField] Text TextField;
    FloatArgument Argument;

    void Start()
    {

        TextField.text = "Success Rate 0%";
        OnSuccessRateChange.RegisterAction(OnSuccessRateChanged);
        OnSceneLoadComplete.RegisterAction(OnSuccessRateChanged);
    }

    public void OnSuccessRateChanged(EventArgs Args)
    {
        Argument = Args as FloatArgument;
        if(Argument!=null)
            TextField.text = "Success Rate "+(Argument.value *100).ToString("0")+"%";
        else
            TextField.text = "Success Rate 0%";
    }

    private void OnDestroy()
    {
        OnSuccessRateChange.UnregisterAction(OnSuccessRateChanged);
        OnSceneLoadComplete.UnregisterAction(OnSuccessRateChanged);
    }
}
