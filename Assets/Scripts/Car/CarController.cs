using System.Collections;
using UnityEngine;
using Pixelplacement; 
public class CarController : MonoBehaviour
{
    
    public delegate void CarArriveCallback(CarController Car,bool success);
    float CarSpeed;
    [SerializeField] Spline ConnectedSpline;
    [SerializeField] SplineFollower Follower;
    public MeshRenderer CarRenderer;
    public CarArriveCallback OnArrive;

    public void SetCarSpeed(float speed)
    {
        CarSpeed = speed;
    }
    public void SetConnectedSpline(Spline spline)
    {
        ConnectedSpline = spline;
    }
    public void StartMovement()
    {
         StartCoroutine(DoMove());
    }
    IEnumerator DoMove()
    {
        Follower = new SplineFollower();
        Follower.faceDirection = true;
        Follower.target = transform;
        ConnectedSpline.followers.Add(Follower);
        while (gameObject.activeSelf)
        { 
            MovePercent( Time.deltaTime*CarSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    public void MovePercent(float amount)
    {
        float left = 1 - Follower.percentage;
        float incomingAmount = amount;
        if (left > incomingAmount)
        {
            Follower.percentage += incomingAmount;

        }
        else
        {
            incomingAmount -= left;
            jumpNext(incomingAmount);
        }


    }
    public void jumpNext(float MoveForward)
    {
        Node LastNode = ConnectedSpline.Anchors[ConnectedSpline.Anchors.Length - 1].GetComponent<Node>();
        ConnectedSpline.followers.Remove(Follower);
        if (LastNode._PathManager.GetCurrentConnection()._PathManager.GetType() == typeof(House))
        {
             bool success = (LastNode._PathManager.GetCurrentConnection()._PathManager as House).CheckHouse(this);
            OnArrive.Invoke(this, success);
            gameObject.SetActive(false);
        }
        else
        {
            ConnectedSpline = LastNode._PathManager.GetCurrentConnection().Master;
            Follower.percentage = 0f;
            ConnectedSpline.followers.Add(Follower);
            MovePercent(MoveForward);
        }
    }
}
