using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Pixelplacement.TweenSystem;

public class House : PathManager
{
    [SerializeField] GameEvent OnHouseScan;
    public MeshRenderer HouseRenderer;
    HouseScanArguments Arguments;
    TweenBase successBounce;
    public  override void Awake()
    {
        OnHouseScan.RegisterAction(HouseScan);
        base.Awake();
    }
    public override void Start()
    {

        base.Start();
    }
    public bool CheckHouse(CarController Car)
    {
        if (Car.CarRenderer.sharedMaterial == HouseRenderer.sharedMaterial)
        {
            successBounce = Tween.Shake(HouseRenderer.transform, Vector3.zero, Vector3.one * 0.05f, 1, 0);
            successBounce.Start();
            return true;
        }
        else
            return false;

    }
    public void HouseScan(EventArgs args)
    { 
     Arguments = args as HouseScanArguments;
        if (Arguments != null)
        {
            Arguments.CallBack(this);
        }
    }
    private void OnDisable()
    {
        OnHouseScan.UnregisterAction(HouseScan);
    }

}
