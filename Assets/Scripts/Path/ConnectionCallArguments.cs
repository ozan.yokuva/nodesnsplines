using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionCallArguments : EventArgs
{
    public delegate void CallBackDelegate(Node Entry);
    public CallBackDelegate CallBack;
    public Vector3 ConnectiorCenter;
    public float ConnectionDistance;


    public ConnectionCallArguments()
    {
    //to setup later;
    }
    public ConnectionCallArguments(Vector3 Center, float Range, CallBackDelegate CallBack)
    { 
        this.ConnectiorCenter = Center;
        this.ConnectionDistance = Range;
        this.CallBack = CallBack;
    
    }
    public void Set(Vector3 Center, float Range, CallBackDelegate CallBack)
    {
        this.ConnectiorCenter = Center;
        this.ConnectionDistance = Range;
        this.CallBack = CallBack;

    }
}
