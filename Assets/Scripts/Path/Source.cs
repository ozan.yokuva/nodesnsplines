using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Random = UnityEngine.Random;

public class Source : StaticPathManager
{
    [SerializeField] Spline BaseSpline;
    [SerializeField] GameObject CarPrefab;
    [SerializeField] GameEvent OnLevelStart;
    [SerializeField] GameEvent OnLevelHousesInit;
    [SerializeField] GameEvent OnLevelFail;
    [SerializeField] GameEvent OnLevelSuccess;
    [SerializeField] GameEvent OnSuccessRateChange;
    List<House> Houses;
    List<CarController> Cars;
    List<CarController> CarsOnTheGo;
    int Successful;
    int Fail;
    float spawnDelay => gameParameters.CarSpawnDelay;
    GameObject TempGameObject;

    public override void Awake()
    {
        OnLevelStart.RegisterAction(OnLevelStarted);
    }
    public override void Start()
    {
        base.Start();
        Houses = new List<House>();
        ScanHouses();
        InstantiateCars();
    }
    public void ScanHouses()
    {
        Houses.Clear();
        OnLevelHousesInit.Invoke(new HouseScanArguments(HouseCallBack));
    }
    public void InstantiateCars()
    {
        Cars = new List<CarController>();
        for (int i = 0; i < Houses.Count * gameParameters.CarMultiplier; i++)
        {
            TempGameObject = Instantiate(CarPrefab);
            TempGameObject.transform.SetParent(transform);
            TempGameObject.transform.position = transform.position;

            CarController carController = TempGameObject.GetComponent<CarController>();
            carController.OnArrive = CarArrive;
            carController.SetCarSpeed(gameParameters.CarSpeed);
            carController.CarRenderer.sharedMaterial = Houses[i% Houses.Count].HouseRenderer.sharedMaterial;
            carController.SetConnectedSpline(BaseSpline);

            Cars.Add(carController);
        }
 

    }
    public void CarArrive(CarController Car,bool success)
    {
        CarsOnTheGo.Remove(Car);

        
        if (success)
            Successful++;
        else
            Fail++;
        float successRate = 0;
        if (Successful > 0)
            successRate = (float)Successful / (Fail + Successful);

       OnSuccessRateChange.Invoke(new FloatArgument(successRate));

        if (CarsOnTheGo.Count == 0 && Cars.Count==0)
        {
            if (Successful < Fail)
                OnLevelFail.Invoke();
            else
                OnLevelSuccess.Invoke();
        }
    }

    public void OnLevelStarted(EventArgs Args )
    {
        StartCoroutine(CarSpawner());
    }
    public void HouseCallBack(House house)
    { 
        Houses.Add(house);
    }

    private void OnDestroy()
    {
        OnLevelStart.UnregisterAction(OnLevelStarted);
    }

    private void OnDisable()
    {
        OnLevelStart.UnregisterAction(OnLevelStarted);
    }
    IEnumerator CarSpawner()
    {
        CarsOnTheGo = new List<CarController>();
        while (Cars.Count > 0)
        { 
            int randomCar = Random.Range(0, Cars.Count);
            Cars[randomCar].StartMovement();
            CarsOnTheGo.Add(Cars[randomCar]);
            Cars.RemoveAt(randomCar);
            yield return new WaitForSeconds(spawnDelay);

        }
    }

}
