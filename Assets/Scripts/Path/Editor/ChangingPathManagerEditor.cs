using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ChanginPathManager))]
public class ChangingPathManagerEditor : PathManagerEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Switch"))
        {
            (target as ChanginPathManager).switchExit();
        }
    }
    
}
