using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathManager),true)]
public class PathManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        if (GUILayout.Button("RotateEntry"))
        {
            (target as PathManager).rotateEntry();
        }
        if (GUILayout.Button("RotateExit"))
        {
            (target as PathManager).rotateExit();
        }
    }
    
}
