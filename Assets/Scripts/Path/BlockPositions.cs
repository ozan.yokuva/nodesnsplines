using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockPositions : MonoBehaviour
{
    
    public List<Vector3> BlockPositionsList;
    public Vector3 getNext(Vector3 current)
    {
        if (BlockPositionsList.Contains(current))
        {
            int indexOfCurrent = BlockPositionsList.IndexOf(current);
            indexOfCurrent = (indexOfCurrent + 1) % BlockPositionsList.Count;
            return BlockPositionsList[indexOfCurrent];
        }
        else
            return BlockPositionsList[0]; 
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        foreach (var block in BlockPositionsList)
        {
            Gizmos.DrawSphere(transform.position + block, 0.05f);
        }
    }
}
