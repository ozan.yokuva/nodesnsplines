using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
//using Dreamteck.Splines;

[RequireComponent(typeof(BlockPositions))]
public class PathManager : MonoBehaviour
{
    [SerializeField] protected GameParameters gameParameters;
    [SerializeField] protected GameEvent OnConnectionCall;
    [SerializeField] protected bool AutoConnect;
    [SerializeField] protected List<GameObject> Tangents;
    [SerializeField] protected Node NodeEntry;
    [SerializeField] protected Node NodeExit;
    protected ConnectionCallArguments ConnectionArguments;
    protected float AutoConnectDistance => gameParameters.NodeConnectDistance;
    BlockPositions BlockPositionsContainer;
    [HideInInspector] public List<Node> Connections;
    public BlockPositions BlockPositionsHolder
    {
        get {
        if(BlockPositionsContainer == null)
                BlockPositionsContainer = GetComponent<BlockPositions>();
        return BlockPositionsContainer;
        }
    }
    public virtual Node GetCurrentConnection()
    {
        return Connections[0];
    }
    public virtual void Awake()
    {
        OnConnectionCall.RegisterAction(OnConnectionCalled);
    }
    public virtual void ResetConnections()
    {
        Debug.Log("ResetConnections");
        if (Connections==null)
            Connections = new List<Node>();

        Connections.Clear();
        ConnectionArguments = new ConnectionCallArguments();
        ConnectionArguments.Set(NodeExit.transform.position, AutoConnectDistance, OnConnectionCallBack);
        OnConnectionCall.Invoke(ConnectionArguments);
       
       
    }
    public virtual void Start()
    {
        if(AutoConnect)
      ResetConnections();
    }
    public void OnConnectionCallBack(Node Entry)
    { 
        Connections.Add(Entry); 
    }
    public void OnConnectionCalled(EventArgs Args)
    {
        ConnectionArguments = Args as ConnectionCallArguments;
        if (ConnectionArguments != null)
        {
            if (isInRange(ConnectionArguments.ConnectiorCenter, ConnectionArguments.ConnectionDistance))
            {
                ConnectionArguments.CallBack(NodeEntry);
            }
        }
    }
    public bool isInRange(Vector3 center, float range)
    {
         return Vector3.Distance(NodeEntry.transform.position, center) <= range;
    }
    public void rotateEntry()
    {
        NodeEntry.transform.localPosition = BlockPositionsHolder.getNext(NodeEntry.transform.localPosition);
        resetTangents();
    }

    public void rotateExit()
    {
        NodeExit.transform.localPosition = BlockPositionsHolder.getNext(NodeExit.transform.localPosition);
        resetTangents();
    }

    public void resetTangents()
    {
        foreach (var Tangent in Tangents)
        {
            Tangent.transform.transform.position = transform.position;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(NodeEntry.transform.position, .1f);
        Gizmos.color = Color.red;
        if(NodeExit!=null)
            Gizmos.DrawWireSphere(NodeExit.transform.position, .12f);

    }
    private void OnDestroy()
    {
        OnConnectionCall.UnregisterAction(OnConnectionCalled);
    }

    public Node GetEntryNode()
    {
        return NodeEntry;
    }

    public Node GetExitNode()
    {
        return NodeExit;
    }

}


