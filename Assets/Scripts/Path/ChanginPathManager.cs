using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChanginPathManager : PathManager
{
    int CurrentExitIndex = 0;
    [SerializeField] SpriteRenderer Rotatable;

    public override void Start()
    {
        ResetConnections();
        switchExit();
    }
    private void OnMouseDown()
    {
        switchExit();
    }
    public void switchExit()
    {
        if (Connections != null && Connections.Count > 0)
        {
            CurrentExitIndex++;
            CurrentExitIndex = CurrentExitIndex % Connections.Count;
            NodeExit.transform.position = Connections[CurrentExitIndex].transform.position;
            foreach (GameObject node in Tangents)
                node.transform.transform.position = transform.position;
        }
    }
    public override Node GetCurrentConnection()
    {
        return Connections[CurrentExitIndex];
    }

    public override void ResetConnections()
    {

        if (Connections == null)
            Connections = new List<Node>();

        Connections.Clear();

        ConnectionArguments = new ConnectionCallArguments();

        foreach (Vector3 position in BlockPositionsHolder.BlockPositionsList)
        {
            if (position != NodeEntry.transform.localPosition)
            {
                ConnectionArguments.Set(transform.position + position, AutoConnectDistance, OnConnectionCallBack);
                OnConnectionCall.Invoke(ConnectionArguments);
            }
        }

        Rotatable.enabled = Connections.Count > 1;


    }
}