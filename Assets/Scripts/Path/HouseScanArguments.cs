using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseScanArguments : EventArgs
{
    public delegate void CallBackDelegate(House Entry);
    public CallBackDelegate CallBack;
   


    public HouseScanArguments()
    {
    }
    public HouseScanArguments(CallBackDelegate CallBack)
    { 
        this.CallBack = CallBack;
    
    }
    public void Set(CallBackDelegate CallBack)
    {
        this.CallBack = CallBack;
    }
}
