using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
public class Node : MonoBehaviour
{
    public Spline Master;
    public PathManager _PathManager;
    public bool isEntry {
        get { return _PathManager.GetEntryNode() == gameObject; }
    }


}
