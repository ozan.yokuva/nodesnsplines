using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="GameParameters" , menuName ="ScriptableObject/GameParameters")]
public class GameParameters : ScriptableObject
{
    public float CarSpeed = 0.5f;
    public int CarMultiplier =1;
    public float CarSpawnDelay =3f;
    public float NodeConnectDistance =0.2f;
    
}
